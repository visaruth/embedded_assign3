#include<reg51.h>
sbit latch = P1^0;

sbit switch_pin0 = P0^0;
sbit switch_pin1 = P0^1;
sbit switch_pin2 = P0^2;
sbit switch_pin3 = P0^3;
sbit switch_pin4 = P0^4;
sbit switch_pin5 = P0^5;
sbit switch_pin6 = P0^6;
sbit switch_pin7 = P0^7;

sbit led_pin0 = P2^0;
sbit led_pin1 = P2^1;
sbit led_pin2 = P2^2;
sbit led_pin3 = P2^3;
sbit led_pin4 = P2^4;
sbit led_pin5 = P2^5;
sbit led_pin6 = P2^6;
sbit led_pin7 = P2^7;

int m = 0;

void main(void) {
	latch = 1;

	if(switch_pin0 == 0) {
		led_pin0 = 0;
	} else if(switch_pin0 == 1) {
		led_pin0 = 1;
	}

	if(switch_pin1 == 0) {
		led_pin1 = 0;
	} else if(switch_pin1 == 1) {
		led_pin1 = 1;
	}

	if(switch_pin2 == 0) {
		led_pin2 = 0;
	} else if(switch_pin2 == 1) {
		led_pin2 = 1;
	}

	if(switch_pin3 == 0) {
		led_pin3 = 0;
	} else if(switch_pin3 == 1) {
		led_pin3 = 1;
	}

	if(switch_pin4 == 0) {
		led_pin4 = 0;
	} else if(switch_pin4 == 1) {
		led_pin4 = 1;
	}

	if(switch_pin5 == 0) {
		led_pin5 = 0;
	} else if(switch_pin5 == 1) {
		led_pin5 = 1;
	}

	if(switch_pin6 == 0) {
		led_pin6 = 0;
	} else if(switch_pin6 == 1) {
		led_pin6 = 1;
	}

	if(switch_pin7 == 0) {
		led_pin7 = 0;
	} else if(switch_pin7 == 1) {
		led_pin7 = 1;
	}
}